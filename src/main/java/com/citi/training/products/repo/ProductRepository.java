package com.citi.training.products.repo;

import java.util.List;

import com.citi.training.products.model.Product;

public interface ProductRepository {

    void save(Product product);

    Product findById(int id);

    List<Product> findAll ();
    
    Product delete(int id);
}
