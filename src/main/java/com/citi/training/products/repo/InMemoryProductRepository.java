package com.citi.training.products.repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.citi.training.products.model.Product;

@Component
public class InMemoryProductRepository implements ProductRepository{

    private HashMap<Integer, Product> allProducts = new HashMap<Integer, Product>();

    public void save(Product product) {
        allProducts.put(product.getId(), product);
    }

    public Product findById(int id) {
        return allProducts.get(id);
    }

    public List<Product> findAll() {
        return new ArrayList<Product>(allProducts.values());
    }

    public Product delete(int id) {
    	return allProducts.remove(id);
    }
}
